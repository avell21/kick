<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">

    <title>Laravel</title>
<body>
<div class="container">
    <div class="row">
        <br><br><br><br>

        <ul>
            @foreach($errors->all() as $message)
                <li><span class="alert alert-danger">{{$message}}</span></li>
            @endforeach
        </ul>
        <div class="col-lg-2 col-lg-offset-5">
            <form action="{{url('submit')}}" method="POST">
                {{csrf_field()}}
                <label for="name">name</label>
                <input type="text"name="name" class="form-control">
                <label for="pass">pass</label>
                <input type="password" name="pass" class="form-control">
                <input type="submit" class="btn btn-danger form-control">
            </form>
        </div>
    </div>

</div>






<script src="{{URL::asset('js/vue.js')}}"></script>
<script src="{{URL::asset('js/app.js')}}"></script>
</body>

</html>
