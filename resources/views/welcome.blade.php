<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }


        </style>
    </head>
    <body>
            <div class="container" id="root" >
                <div class="jumbotron">
                    <form action="{{ url('sign-up') }}" method="POST" class="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <h2>@{{user}}</h2>
                        <input type="text" class="form-controller" placeholder="name" name="name" v-model="user">
                        <input type="password" class="form-controller" placeholder="password" name="password">
                        <input type="file" class="form-controller" name="video">
                        <input type="submit" value="submit"  class="btn btn-success">
                    </form>
                </div>

            </div>
        <script src="{{URL::asset('js/vue.js')}}"></script>
        <script src="{{URL::asset('js/main.js')}}"></script>

    </body>

</html>
