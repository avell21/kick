<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// TODO get the file to be

Route::get('/', function () {
    return view('welcome');
});

Route::get('/vue',function (){
    return view('test');
});
Route::get('/t',function (){
    $pands =\App\pand::all();
    return $pands;
});
Route::post('/sign-up',function (){
    if(Request::file('video')){
    $name = Request::file('video')->getClientOriginalName();
    Request::file('video')->move('storage/directory',$name);
    return "file was successfully moved";
    }
    else{
        echo "sorry but there is no file";
    }
});
Route::get('princess/{name}',function ( $name ){
 return "sorry princess {$name}" ;
})->where('name','[A-Za-z]+');

Route::get('shit','testController@index');

Route::resource('user','test2Controller');

Route::get('form',function (){
    return view('form');
});
Route::post('/submit',function (Request $request){
    $data = $request :: All();
    $rules = [
        'name' => 'alpha_num'
    ];
    $valida = Validator::make($data,$rules);

    if($valida->passes()){
        return $data;
    }
    return redirect('form')->withErrors($valida);
});